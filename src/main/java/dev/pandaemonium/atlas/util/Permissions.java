package dev.pandaemonium.atlas.util;

import dev.pandaemonium.atlas.Atlas;
import dev.pandaemonium.atlas.config.AtlasConfig;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public enum Permissions {

    TPACCEPT("atlas.command.tpaccept", config().tpaBasePerm),
    TPADENY("atlas.command.tpadeny", config().tpaBasePerm),
    TPA("atlas.command.tpa", null, config().tpaBasePerm, TPACCEPT, TPADENY),
    BYPASS_COOLDOWN("atlas.bypass.cooldowns", null, config().skipAll),
    BYPASS_ALL("atlas.bypass.cooldowns", null, config().skipAll, BYPASS_COOLDOWN),
    PAN("pandaemonium.command.pan", "The Pandemonium central permission", PermissionDefault.TRUE),
    ALL("atlas.admin", "An over-arching administrator permission.", PermissionDefault.OP, Permissions.values());

    private static AtlasConfig config = null;
    private final String label;
    private final PermissionDefault scope;
    private final String description;
    private final Permissions[] children;

    Permissions(String permission) {
        this.label = permission;
        this.scope = Atlas.getInstance().getDescription().getPermissionDefault();
        this.description = null;
        this.children = new Permissions[0];
    }

    Permissions(String permission, PermissionDefault scope) {
        this.label = permission;
        this.scope = scope;
        this.description = null;
        this.children = new Permissions[0];
    }

    Permissions(String permission, String description, PermissionDefault scope) {
        this.label = permission;
        this.scope = scope;
        this.description = description;
        this.children = new Permissions[0];
    }

    Permissions(String permission, String description, PermissionDefault scope, Permissions... children) {
        this.label = permission;
        this.scope = scope;
        this.description = description;
        this.children = children;
    }

    public String getName() {
        return name();
    }

    @Override
    public String toString() {
        return label;
    }

    public String getLabel() {
        return label;
    }

    public PermissionDefault getScope() {
        return scope;
    }

    public @Nullable String getDescription() {
        return description;
    }

    public Permissions[] getChildren() {
        return children;
    }

    public static void register() {
        for (Permissions value : values()) {
            Map<String, Boolean> map = new HashMap<>();
            for (Permissions child : value.children) {
                map.put(child.label, true);
            }
            Permission permission = new Permission(value.label, value.description, value.scope, map);
            Bukkit.getPluginManager().addPermission(permission);
        }
    }

    private static AtlasConfig config() {
        if (config == null) config = Atlas.getInstance().config;
        return config;
    }

}
