package dev.pandaemonium.atlas.util.timing;

import dev.pandaemonium.atlas.Atlas;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class Ticker {

    protected final long length;
    protected final long delayBetween;
    protected final int totalTickCount;
    volatile AtomicInteger iteration;
    boolean running;

    public Ticker(long length, long delayBetween) {
        this.length = length;
        this.delayBetween = delayBetween;
        this.totalTickCount = (int) (length/ delayBetween);
        this.running = false;
        this.iteration = new AtomicInteger(0);
    }

    public int getIteration() {
        return iteration.get();
    }

    public long getLength() {
        return length;
    }

    public long getDelayBetween() {
        return delayBetween;
    }

    public int getTotalTickCount() {
        return totalTickCount;
    }

    public int getRemaining() {
        return totalTickCount - getIteration();
    }

    public boolean isRunning() {
        return running;
    }

    public abstract void iterate();

    public void run() {
        if (running) return;
        running = true;
        iteration = new AtomicInteger(0);
        for (long i = 0; i < totalTickCount; i++) {
            long j = i * delayBetween;
            new BukkitRunnable() {
                @Override
                public void run() {
                    iteration.getAndIncrement();
                    Ticker.this.run();
                }
            }.runTaskLater(Atlas.getInstance(), j);
        }
        running = false;
    }

}
