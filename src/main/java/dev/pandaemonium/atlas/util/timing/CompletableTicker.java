package dev.pandaemonium.atlas.util.timing;

import dev.pandaemonium.atlas.Atlas;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class CompletableTicker extends Ticker {

    public CompletableTicker(long length, long delayBetween) {
        super(length, delayBetween);
    }

    public void run() {
        if (isRunning()) return;
        running = true;
        iteration = new AtomicInteger(0);
        for (long i = 0; i < totalTickCount; i++) {
            long j = i * delayBetween;
            new BukkitRunnable() {
                @Override
                public void run() {
                    iteration.getAndIncrement();
                    CompletableTicker.this.run();
                }
            }.runTaskLater(Atlas.getInstance(), j);
        }
        onCompletion();
        running = false;
    }

    public abstract void onCompletion();

}
