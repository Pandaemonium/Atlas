package dev.pandaemonium.atlas.command;

import com.moderocky.mask.command.Commander;
import com.moderocky.mask.template.WrappedCommand;
import dev.pandaemonium.commons.core.Pandaemonium;
import dev.pandaemonium.commons.resource.Branding;
import dev.pandaemonium.commons.resource.PanResource;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class PandaemoniumCommand extends Commander<CommandSender> implements WrappedCommand {

    private final BaseComponent[] prefix = ComponentSerializer.parse(Branding.PREFIX);

    @Override
    public @NotNull Main create() {
        return command("pan");
    }

    @Override
    public @NotNull Consumer<CommandSender> getDefault() {
        Pandaemonium pan = Pandaemonium.getInstance();
        ComponentBuilder builder = new ComponentBuilder("");
        for (PanResource resource : pan.getResources()) {
            builder.append(System.lineSeparator()).reset()
                    .append(" - ").color(ChatColor.WHITE)
                    .append(resource.getName())
                    .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(resource.getResourceDescription()).create()))
                    .color(resource.getPanVersion().equalsIgnoreCase(pan.getVersion()) ? ChatColor.GRAY : ChatColor.DARK_GRAY);
        }
        return sender -> sender.sendMessage(new ComponentBuilder("")
                .append(prefix)
                .append(" ").color(ChatColor.WHITE)
                .append("General Information:")
                .append(System.lineSeparator()).reset()
                .append("Loaded Resources:").color(ChatColor.GRAY)
                .append(" " + pan.getResourceCount())
                .color(ChatColor.GOLD)
                .append(System.lineSeparator()).reset()
                .append("Loaded Modules:").color(ChatColor.GRAY)
                .append(" " + pan.getModuleCount())
                .color(ChatColor.GOLD)
                .append(System.lineSeparator()).reset()
                .append(System.lineSeparator()).reset()
                .append("Resource List:").color(ChatColor.GRAY)
                .append(builder.create())
                .create()
        );
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("pandemonium", "pandaemonium");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "The command for all Pandaemonium resources.";
    }

    @Override
    public @Nullable String getPermission() {
        return "pandaemonium.command.pan";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return null;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        return execute(sender, args);
    }
}
