package dev.pandaemonium.atlas.command;

import com.moderocky.mask.command.Commander;
import com.moderocky.mask.template.WrappedCommand;
import dev.pandaemonium.atlas.Atlas;
import dev.pandaemonium.atlas.config.AtlasConfig;
import dev.pandaemonium.atlas.util.Messenger;
import dev.pandaemonium.atlas.util.Permissions;
import dev.pandaemonium.atlas.util.timing.CompletableTicker;
import dev.pandaemonium.atlas.util.timing.Ticker;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public class TpacceptCommand extends Commander<Player> implements WrappedCommand {

    private final AtlasConfig config = Atlas.getInstance().config;
    private final Messenger messenger = Atlas.getInstance().getMessenger();

    @Override
    public @NotNull Main create() {
        return command("tpaccept");
    }

    @Override
    public @NotNull Consumer<Player> getDefault() {
        return player -> {
            if (hasReceived(player)) {
                Player target = getReceived(player);
                messenger.sendMessage(new ComponentBuilder("")
                                .append("Accepting " + target.getName() + "'s teleport request.")
                                .create(),
                        player);
                messenger.sendMessage(new ComponentBuilder("")
                                .append(player.getName() + " has accepted your teleport request.")
                                .create(),
                        target);
                int delay = config.tpaDelayTime;
                if (delay > 0 && !player.hasPermission(Permissions.BYPASS_COOLDOWN.getLabel())) {
                    Location location = player.getLocation();
                    Ticker ticker = new CompletableTicker(config.tpaDelayTime * 20, 20) {
                        @Override
                        public void onCompletion() {
                            if (!player.isOnline() || !target.isOnline()) return;
                            if (target.getLocation().distanceSquared(location) > config.tpaMoveAmount * config.tpaMoveAmount) {
                                messenger.sendMessage(new ComponentBuilder("")
                                                .append("You moved too much.")
                                                .create(),
                                        target);
                                messenger.sendMessage(new ComponentBuilder("")
                                                .append(target.getDisplayName() + " moved too much.")
                                                .create(),
                                        player);
                            } else {
                                messenger.sendMessage(new ComponentBuilder("")
                                                .append("Teleporting...")
                                                .create(),
                                        target, player);
                                target.teleport(player);
                                target.setNoDamageTicks(config.genTeleportNoDamage);
                            }
                        }

                        @Override
                        public void iterate() {
                            target.sendActionBar(config.highlightColour.toString() + "Teleporting in: " + getRemaining() + "s");
                        }
                    };
                    messenger.sendMessage(new ComponentBuilder("")
                                    .append("Preparing to teleport in ")
                                    .color(config.textColour)
                                    .append(config.tpaDelayTime + "s")
                                    .color(config.highlightColour)
                                    .append(". Stand still!")
                                    .color(config.textColour)
                                    .create(),
                            target);

                    ticker.run();
                } else {
                    messenger.sendMessage(new ComponentBuilder("")
                                    .append("Teleporting...")
                                    .create(),
                            target, player);
                    target.teleport(player);
                    target.setNoDamageTicks(config.genTeleportNoDamage);
                }
            } else {
                player.sendMessage(new ComponentBuilder("")
                        .append(Atlas.getInstance().getLabel())
                        .append("You have no pending teleport requests.")
                        .create()
                );
            }
        };
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("teleportaccept", "tpaaccept");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "Accept a teleport request from another player.";
    }

    @Override
    public @Nullable String getPermission() {
        return "atlas.command.tpaccept";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return "You do not have permission for this action!";
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player) return execute((Player) sender, args);
        sender.sendMessage(new ComponentBuilder("")
                .append(Atlas.getInstance().getLabel())
                .append("Only players may use this command!")
                .create()
        );
        return true;
    }

    private void setSent(Player player, Player target) {
        player.setMetadata("sent_tp_request", Atlas.getMetaValue(target.getUniqueId().toString()));
    }

    private void setReceived(Player player, Player from) {
        player.setMetadata("rec_tp_request", Atlas.getMetaValue(from.getUniqueId().toString()));
    }

    private boolean hasSent(Player player, Player target) {
        return player.hasMetadata("sent_tp_request") && player.getMetadata("sent_tp_request").get(0).asString().equalsIgnoreCase(target.getUniqueId().toString());
    }

    private boolean hasReceived(Player player, Player from) {
        return player.hasMetadata("rec_tp_request") && player.getMetadata("rec_tp_request").get(0).asString().equalsIgnoreCase(from.getUniqueId().toString());
    }

    private boolean hasReceived(Player player) {
        return player.hasMetadata("rec_tp_request");
    }

    private boolean hasSent(Player player) {
        return player.hasMetadata("sent_tp_request");
    }

    private Player getReceived(Player player) {
        return Bukkit.getPlayer(UUID.fromString(player.getMetadata("rec_tp_request").get(0).asString()));
    }

    private Player getSent(Player player) {
        return Bukkit.getPlayer(UUID.fromString(player.getMetadata("sent_tp_request").get(0).asString()));
    }

}
