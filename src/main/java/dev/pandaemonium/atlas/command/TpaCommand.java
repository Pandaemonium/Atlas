package dev.pandaemonium.atlas.command;

import com.moderocky.mask.command.ArgPlayer;
import com.moderocky.mask.command.Commander;
import com.moderocky.mask.gui.ItemFactory;
import com.moderocky.mask.gui.PaginatedGUI;
import com.moderocky.mask.template.WrappedCommand;
import dev.pandaemonium.atlas.Atlas;
import dev.pandaemonium.atlas.config.AtlasConfig;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class TpaCommand extends Commander<Player> implements WrappedCommand {

    private final AtlasConfig config = Atlas.getInstance().config;

    @Override
    public @NotNull Main create() {
        return command("tpa")
                .arg(
                        (player, input) -> {
                            Player target = (Player) input[0];
                            if (hasSent(player, target)) {
                                player.sendMessage(new ComponentBuilder("")
                                        .append(Atlas.getInstance().getLabel())
                                        .append("You have already sent a request to this player.")
                                        .create()
                                );
                            } else {
                                setSent(player, target);
                                setReceived(target, player);
                                player.sendMessage(new ComponentBuilder("")
                                        .append(Atlas.getInstance().getLabel())
                                        .append("Sent a request to " + target.getName() + ".")
                                        .create()
                                );
                                target.sendMessage(new ComponentBuilder("")
                                        .append(Atlas.getInstance().getLabel())
                                        .append("Received a teleport request from " + player.getName() + ".")
                                        .append(System.lineSeparator())
                                        .append("Accept? ").color(config.textColour)
                                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Click to accept.")))
                                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpaccept"))
                                        .append("/tpaccept").color(config.highlightColour)
                                        .append(System.lineSeparator())
                                        .append("Reject? ").color(config.textColour)
                                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, TextComponent.fromLegacyText("Click to reject.")))
                                        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/tpdeny"))
                                        .append("/tpdeny").color(config.highlightColour)
                                        .create()
                                );
                            }
                        },
                        new ArgPlayer()
                );
    }

    @Override
    public @NotNull Consumer<Player> getDefault() {
        return player -> {
            PaginatedGUI gui = new PaginatedGUI(Atlas.getInstance(), 54, "Online Players")
                    .setLayout(new String[]{
                            "#########",
                            "#########",
                            "#########",
                            "#########",
                            "#########",
                            "<OOOVOOO>"
                    });
            gui.createButton('<', new ItemFactory(Material.ARROW)
                    .addConsumer(meta -> meta.setDisplayName("§fPrev. Page"))
                    .create(), gui.getPageDown());
            gui.createButton('>', new ItemFactory(Material.ARROW)
                    .addConsumer(meta -> meta.setDisplayName("§fNext Page"))
                    .create(), gui.getPageUp());
            gui.createButton('V', new ItemFactory(Material.ARROW)
                    .addConsumer(meta -> meta.setDisplayName("§fFirst Page"))
                    .create(), gui.getPageReset());
            gui.createTile('O', new ItemStack(Material.AIR));
            gui.setEntryChar('#');
            gui.setEntryConsumer((player1, event) -> {
                ItemStack stack = event.getCurrentItem();
                if (stack == null || !(stack.getItemMeta() instanceof SkullMeta)) return;
                SkullMeta meta = (SkullMeta) stack.getItemMeta();
                OfflinePlayer target = meta.getOwningPlayer();
                if (target != null) player1.performCommand("tpa " + target.getName());
            });
            List<ItemStack> itemStacks = new ArrayList<>();
            for (Player online : Bukkit.getOnlinePlayers()) {
                itemStacks.add(new ItemFactory(Material.PLAYER_HEAD)
                        .addConsumer(meta -> {
                            SkullMeta skull = (SkullMeta) meta;
                            skull.setOwningPlayer(online);
                            skull.setDisplayName(online.getDisplayName());
                            PersistentDataContainer container = meta.getPersistentDataContainer();
                            container.set(Atlas.getNamespacedKey("player"), PersistentDataType.STRING, player.getUniqueId().toString());
                        })
                        .create());
            }
            gui.setEntries(itemStacks);
            gui.open(player);
        };
    }

    @Override
    public @NotNull List<String> getAliases() {
        return Arrays.asList("teleporta", "tpto");
    }

    @Override
    public @NotNull String getUsage() {
        return "/" + getCommand();
    }

    @Override
    public @NotNull String getDescription() {
        return "Request to teleport to another player.";
    }

    @Override
    public @Nullable String getPermission() {
        return "atlas.command.tpa";
    }

    @Override
    public @Nullable String getPermissionMessage() {
        return "You do not have permission for this action!";
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player) return execute((Player) sender, args);
        sender.sendMessage(new ComponentBuilder("")
                .append(Atlas.getInstance().getLabel())
                .append("Only players may use this command!")
                .create()
        );
        return false;
    }

    private void setSent(Player player, Player target) {
        player.setMetadata("sent_tp_request", Atlas.getMetaValue(target.getUniqueId().toString()));
    }

    private void setReceived(Player player, Player from) {
        player.setMetadata("rec_tp_request", Atlas.getMetaValue(from.getUniqueId().toString()));
    }

    private boolean hasSent(Player player, Player target) {
        return player.hasMetadata("sent_tp_request") && player.getMetadata("sent_tp_request").get(0).asString().equalsIgnoreCase(target.getUniqueId().toString());
    }

    private boolean hasReceived(Player player, Player from) {
        return player.hasMetadata("rec_tp_request") && player.getMetadata("rec_tp_request").get(0).asString().equalsIgnoreCase(from.getUniqueId().toString());
    }

}
