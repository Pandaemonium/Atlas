package dev.pandaemonium.atlas.config;

import com.moderocky.mask.annotation.Configurable;
import com.moderocky.mask.template.Config;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

public class AtlasConfig implements Config {

    @Configurable(section = "branding")
    @Configurable.Keyed(nodeName = "help_format")
    @Configurable.Comment(text = {
            "The format used by command response messages.",
            "%p references the label, %m represents the message.",
            "Highlights will be automatically applied."
    })
    public String format = "‹%p› ";

    @Configurable(section = "branding")
    @Configurable.Keyed(nodeName = "label")
    @Configurable.Comment(text = {
            "The label used for command messages."
    })
    public String labelPrefix = "Atlas";

    @Configurable(section = "branding")
    @Configurable.Comment(text = {"The colour used for command labels."})
    public ChatColor labelColour = ChatColor.RED;

    @Configurable(section = "branding")
    @Configurable.Comment(text = {"The colour used for highlights."})
    public ChatColor highlightColour = ChatColor.WHITE;

    @Configurable(section = "branding")
    @Configurable.Comment(text = {
            "The colour used for darker formatting."
    })
    public ChatColor softColour = ChatColor.DARK_GRAY;

    @Configurable(section = "branding")
    @Configurable.Comment(text = {
            "The colour used for general text in commands.",
            "A neutral colour is recommended, but you can go wild! :)"
    })
    public ChatColor textColour = ChatColor.GRAY;

    // General features

    @Configurable(section = "features.general")
    @Configurable.Keyed(nodeName = "teleport_damage_protection")
    @Configurable.Comment(text = {
            "The invulnerable time after a teleport.",
            "In ticks (20 = 1 second)."
    })
    public int genTeleportNoDamage = 30;

    @Configurable(section = "features.general")
    @Configurable.Keyed(nodeName = "bypass_perm")
    @Configurable.Comment(text = {
            "The permission state to bypass features such as cool-downs.",
            "TRUE/OP/FALSE"
    })
    public PermissionDefault skipAll = PermissionDefault.OP;

    // TPA features

    @Configurable(section = "features.tpa")
    @Configurable.Keyed(nodeName = "enabled")
    public boolean tpaEnabled = true;

    @Configurable(section = "features.tpa")
    @Configurable.Keyed(nodeName = "permission_default")
    @Configurable.Comment(text = {
            "The basic permission state.",
            "TRUE/OP/FALSE"
    })
    public PermissionDefault tpaBasePerm = PermissionDefault.TRUE;

    @Configurable(section = "features.tpa")
    @Configurable.Keyed(nodeName = "delay_time")
    public int tpaDelayTime = 5;

    @Configurable(section = "features.tpa")
    @Configurable.Keyed(nodeName = "move_allowance")
    @Configurable.Comment(text = {
            "How far a player may move while waiting to teleport."
    })
    public double tpaMoveAmount = 1.5;

    public AtlasConfig() {
        load();
    }

    @Override
    public @NotNull String getFolderPath() {
        return "plugins/Atlas/";
    }

    @Override
    public @NotNull String getFileName() {
        return "config.yml";
    }

}
