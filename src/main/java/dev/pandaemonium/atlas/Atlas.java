package dev.pandaemonium.atlas;

import com.moderocky.mask.template.BukkitPlugin;
import dev.pandaemonium.atlas.command.PandaemoniumCommand;
import dev.pandaemonium.atlas.config.AtlasConfig;
import dev.pandaemonium.atlas.util.Messenger;
import dev.pandaemonium.atlas.util.Permissions;
import dev.pandaemonium.commons.core.Pandaemonium;
import dev.pandaemonium.commons.resource.PanModule;
import dev.pandaemonium.commons.resource.PanResource;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("NullableProblems")
public class Atlas extends BukkitPlugin implements PanResource {

    public static Atlas instance;

    public final AtlasConfig config = new AtlasConfig();
    private Messenger messenger;

    @Override
    public void startup() {
        instance = this;
        registerCommons();
        messenger = new Messenger(this);
        Permissions.register();
    }

    @Override
    public void disable() {
        instance = null;
    }

    private void registerCommons() {
        Pandaemonium.getInstance().register(this);
        if (isRegistered("pandaemonium")) return;
        register(new PandaemoniumCommand());
        Bukkit.getPluginManager().addPermission(new Permission("pandaemonium.command.pan", PermissionDefault.TRUE));
    }

    @Override
    public @NotNull String getID() {
        return "atlas";
    }

    @Override
    public @NotNull String getResourceDescription() {
        return getPluginDescription();
    }

    @Override
    public @NotNull String getPanVersion() {
        return "1.0.0";
    }

    @Override
    public @NotNull List<PanModule> getModules() {
        return new ArrayList<>();
    }

    public static Atlas getInstance() {
        return instance;
    }

    public BaseComponent[] getLabel() {
        String string = config.format;
        if (string.contains("%p")) {
            string = config.softColour + config.format.split("%p")[0]
                    + config.labelPrefix + config.format.split("%p")[1];
        }
        string = string + config.highlightColour;
        return TextComponent.fromLegacyText(string, config.textColour);
    }

    public Messenger getMessenger() {
        return messenger;
    }
}
